syms x
t(x) = x^2 + 4*sin(x);
x0 = 1;
%2 член
t1 = taylor(t, x, x0, 'Order', 2)
p1 = sym2poly(t1)
%3 член
t2 = taylor(t, x, x0, 'Order', 3)
p2 = sym2poly(t2)
%4 член
t3 = taylor(t, x, x0, 'Order', 4)
p3 = sym2poly(t3)
% 5 член
t4 = taylor(t, x, x0, 'Order', 5)
p4 = sym2poly(t4)
x1 = x0 -5 : 0.01 : x0 + 5;
y1 = polyval(p1, x1);
y2 = polyval(p2, x1);
y3 = polyval(p3, x1);
y4 = polyval(p4, x1);
figure
hold on
grid on
plot(x1, t(x1), 'k', 'LineWidth', 2)
plot(x1, y1, 'g');
plot(x1, y2, 'b');
plot(x1, y3, 'r');
plot(x1, y4, 'm');
plot(x0, t(x0), 'rx')
legend('y(x)','2 слагаемых','3 слагаемых','4 слагаемых','5 слагаемых','Location','NorthWest' )
title('Рис1.Графики разложения в ряд Тейлора')
xlabel('x')
ylabel('y')

