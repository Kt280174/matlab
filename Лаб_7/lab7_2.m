x = sym('x');
rootsVPA = solve(x^3 + 5.6 * x^2 + 10.11 * x + 5.8, x);
rootsVPA1 = vpa(sort(rootsVPA1), 15)
%fzero
x = fzero(@pol1,[-2.5 -2.2]);
x(2) = fzero(@pol1, [-2.1 -2]);
x(3) = fzero(@pol1, [-2 -1])
%pol
P =[1 5.6 10.11 5.8];
rootsp = roots(P);
rootsp = sort(rootsp)
%delta 
for i = 1 : 3
    fprintf("%d Delta1 = %.15f \n", i, abs(rootsVPA1(i) - x(i)))
end
for i = 1 : 3
    fprintf("%d Delta2 = %.15f \n", i, abs(rootsVPA1(i) - rootsp(i)))
end
for i = 1 : 3
    fprintf("%d Delta3 = %.15f \n", i, abs(x(i) - rootsp(i)))
end
