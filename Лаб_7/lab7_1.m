%457
x = sym('x');
b = sym('b');
a = sym('a');
f = sqrt((x + a)*(x + b)) - x;
fprintf("No 457 \n");
fprintf("Функция = \n");
pretty(f);
x0 = Inf;
fis= str2sym('sqrt((x + a)*(x + b)) - x');
A = limit(fis, 'x', x0);
fprintf("Предел = \n");
pretty(A);
%496
x = sym('x');
f = (tan(x)^3 - 3*tan(x))/cos(x + pi/6);
fprintf("No 496\n");
fprintf("Функция =\n");
pretty(f);
x0 = pi/3;
fis = str2sym('(tan(x)^3 - 3*tan(x))/cos(x + pi/6)');
A = limit(fis, 'x', x0);
fprintf("Предел =\n");
pretty(A);
%548
x = sym('x');
a = sym('a');
b = sym('b');
c = sym('c');
f = (x^a - c^a)/ (x^b - c^b);
fprintf("No 548\n");
fprintf("Функция = \n");
pretty(f);
x0 = c;
fis = str2sym('(x^a - c^a)/ (x^b - c^b)');
A = limit(fis, 'x', x0);
fprintf("Предел = \n");
pretty(A);
%854
f = x * sqrt(1 + x^2);
fprintf("No 854\n");
fprintf("Функция = \n");
pretty(f);
fis = str2sym('x * sqrt(1 + x^2)');
A = diff(fis,'x', 1);
A1 = simplify(A);
fprintf("Производная = \n");
pretty(A1);
%914
f = acos((1 - x)/sqrt(2));
fprintf("No 914 \n");
fprintf("Функция = \n");
pretty(f);
fis = str2sym('arccos((1 - x)/sqrt(2))');
A = diff(fis, 'x', 1);
A1 = simplify(A);
fprintf("Производная =\n");
pretty(A1);
%1344
f = x^(x^x) - 1;
fprintf("No 1344\n");
fprintf("Функция = \n");
pretty(f);
fis = str2sym('(x^x)^x - 1');
fprintf("Предел = \n");
x0 = 0;
B = limit(fis, 'x', x0);
pretty(B);







