a=3;
b=3.4;
x0=1.68;
y0=-0.17;
x=-1.32:0.05:4.68;
z=-3.57:0.05:3.23;
[X,Z]=meshgrid(x,z);
Yc1=sqrt(1-(X-x0).^2/a^2)*b;
Yc2=-sqrt(1-(X-x0).^2/a^2)*b;
surf(X, Yc1, Z);
hold on
surf(X, Yc2, Z);
colormap spring
shading interp
xlabel('x')
ylabel('y')
zlabel('z')
title('Рис 1. Эллиптический цилидр')