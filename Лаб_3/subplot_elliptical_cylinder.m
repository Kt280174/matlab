a=3;
b=3.4;
x0=1.68;
y0=-0.17;
x=-1.32:0.05:4.68;
z=-3.57:0.05:3.23;
[X,Z]=meshgrid(x,z);
Yc1=sqrt(1-(X-x0).^2/a^2)*b;
Yc2=-sqrt(1-(X-x0).^2/a^2)*b;
s1 = [4 5]
s2 = [10 30]
figure
subplot(2,2,1)
surf(X, Yc1, Z);
hold on
surf(X, Yc2, Z);
view(40,50)
shading interp
xlabel('x')
ylabel('y')
zlabel('z')
title('Эллиптический цилиндр view(40,50)')
subplot(2,2,2)
surf(X, Yc1, Z);
hold on
surf(X, Yc2, Z);
view(30, 100)
shading interp
xlabel('x')
ylabel('y')
zlabel('z')
title('Эллиптический цилиндр view(30,100)')
subplot(2,2,3)
surf(X, Yc1, Z);
hold on
surf(X, Yc2, Z);
view(40,50)
shading interp
xlabel('x')
ylabel('y')
zlabel('z')
title('Эллиптический цилиндр view(40,50)')
subplot(2,2,4)
surf(X, Yc1, Z);
hold on
surf(X, Yc2, Z);
view(30,100)
shading interp
xlabel('x')
ylabel('y')
zlabel('z')
title('Эллиптический цилиндр view(30,100) ')