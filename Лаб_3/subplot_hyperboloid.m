a = 2.19;
b = 8.82;
c = -0.2;
x0 = -0.63;
y0 = 1.06;
z0 = -0.63;
x = -2.9 : 0.05 : 1.7;
y = -8: 0.05 : 10;
[X Y] = meshgrid(x, y);
Z1 = c*sqrt((X - x0).^2/a^2 + (Y - y0).^2/b^2-1) + z0;
Z2 = -c*sqrt((X - x0).^2/a^2 + (Y - y0).^2/b^2-1) + z0;
ind = abs(imag(Z1)) > 0;
Z1(ind) = NaN;
ind  = abs(imag(Z2)) > 0;
Z2(ind) = NaN;
s11 = subplot(2,2,1)
surf(X, Y, Z1);
hold on
surf(X, Y, Z2);
view(50,150)
xlabel('x')
ylabel('y')
zlabel('z')
title('Однополосный гиперболоид')
colormap(s11, hot)
s12 = subplot(2,2,2)
surf(X, Y, Z1);
hold on
surf(X, Y, Z2);
view(20,100)
xlabel('x')
ylabel('y')
zlabel('z')
title('Однополосный гиперболоид')
colormap(s12, cool)
s13 = subplot(2,2,3)
surf(X, Y, Z1);
hold on
surf(X, Y, Z2);
view(30,50)
xlabel('x')
ylabel('y')
zlabel('z')
title('Однополосный гиперболоид')
colormap(s13, jet)
s14 = subplot(2,2,4)
contour(x,y,Z1,10)
hold on
contour(x,y,Z2,10)
xlabel('x')
ylabel('y')
zlabel('z')
title('Контур однополосного гиперболоида')
colormap(s14, gray)
