figure
a = 2.19;
b = 8.82;
c = -0.2;
x0 = -0.63;
y0 = 1.06;
z0 = -0.63;
x = -2.9 : 0.01 : 1.7;
y = -8: 0.1 : 10;
[X Y] = meshgrid(x, y);
Z1 = c*sqrt((X - x0).^2/a^2 + (Y - y0).^2/b^2-1) + z0;
Z2 = -c*sqrt((X - x0).^2/a^2 + (Y - y0).^2/b^2-1) + z0;
ind = abs(imag(Z1)) > 0;
Z1(ind) = NaN;
ind  = abs(imag(Z2)) > 0;
Z2(ind) = NaN;
surf(X, Y, Z1);
hold on
surf(X, Y, Z2);
colormap autumn(5)
xlabel('x')
ylabel('y')
zlabel('z')
title('Рис2.Однополосный гиперболоид')