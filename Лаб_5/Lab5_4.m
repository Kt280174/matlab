p = [-1.11 -5.91 -1.62 11.69 4.19 5.4 10.02 3.16 2 -2.31 -2.74 -2.43];
x = -5 : 0.01 : 4;
r = roots(p);
y = polyval(p, x);
bol = imag(r) == 0;
x0 = r(bol);

p1 = polyder(p);
y1 = polyval(p1, x);
r1 = roots(p1);
bol = imag(r1) == 0;
x01 = r1(bol);

p2 = polyder(p1);
y2 = polyval(p2, x);
r2 = roots(p2);
bol = imag(r2) == 0;
x02 = r2(bol);

p3 = polyder(p2);
y3 = polyval(p2, x);
r3 = roots(p3);
bol = imag(r3) == 0;
x03 = r3(bol);

figure
subplot(4,1,1)
hold on
grid on
axis([-5 4 -10000000 10000000])
plot(x, y)
plot(x0, 0, 'r*')
title('График полинорма')
xlabel('x')
ylabel('y')
subplot(4,1,2)
hold on
grid on
axis([-5 4 -10000000 10000000])
plot(x, y1)
plot(x01, 0, 'r*')
title('График производной первого порядка')
xlabel('x')
ylabel('y')
subplot(4,1,3)
hold on
grid on
axis([-5 4 -10000000 10000000])
plot(x, y2)
plot(x02, 0, 'r*')
title('График производной второго порядка')
xlabel('x')
ylabel('y')
subplot(4,1,4)
hold on
grid on
axis([-5 4 -10000000 10000000])
plot(x, y3)
plot(x03, 0, 'r*')
title('График производной третьего порядка')
xlabel('x')
ylabel('y')