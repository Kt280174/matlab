p = [-1.11 -5.91 -1.62 11.69 4.19 5.4 10.02 3.16 2 -2.31 -2.74 -2.43];
%roots
p1 = polyder(p);
r1 = roots(p1);
bol = imag(r1) == 0;
x01 = r1(bol);
x01 = sort(x01);
% fminsearch
xex(1) = fminsearch(@pol1, -4);
xex(2)= fminsearch(@pol2, -1.5);
xex(3) = fminsearch(@pol1, -1);
xex(4)= fminsearch(@pol2, -0.5);
xex(5) = fminsearch(@pol1, 0.5);
xex(6)= fminsearch(@pol2, 1.3);
%сравнить
delta = vpa(x01 - xex', 15 )

