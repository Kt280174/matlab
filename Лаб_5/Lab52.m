figure
subplot(2,1,1)
p = [-1.11 -5.91 -1.62 11.69 4.19 5.4 10.02 3.16 2 -2.31 -2.74 -2.43];
x = -5:0.1:-1;
y = polyval(p, x);
r = roots(p);
y0 = polyval(p,r);
bol = imag(r) == 0;
x0 = r(bol);
bol1 = x0 < -0.5 & x0 > -3;
x01 = x0(bol1);
plot(x, y);
grid on
hold on
plot(x01, 0, '*r')
grid on
hold on
title('polynom')
xlabel('X')
ylabel('Y')
subplot(2,1,2)
p = [-1.11 -5.91 -1.62 11.69 4.19 5.4 10.02 3.16 2 -2.31 -2.74 -2.43];
x = 1:0.01:2;
y = polyval(p, x);
r = roots(p);
y0 = polyval(p,r);
bol = imag(r) == 0;
x0 = r(bol);
bol2 = x0 <2 & x0 > 1;
x02 = x0(bol2);
plot(x, y);
grid on
hold on
plot(x02, 0, 'r*');
grid on
hold on
title('polynom')
xlabel('X')
ylabel('Y')
