figure
p = [-1.11 -5.91 -1.62 11.69 4.19 5.4 10.02 3.16 2 -2.31 -2.74 -2.43];
x = -10 : 0.01 : 10;
y= polyval(p,x);
plot(x,y);
r = roots(p);
y0 = polyval(p, r);
bol = imag(r) == 0;
x0 = r(bol)
hold on
grid on
plot(x0, 0, '*r');
title('polynom')
xlabel('X')
ylabel('Y')