function y = polinom(x)
    y = x .^3 + 5.6 * x .^2 + 10.11 * x + 5.8;
end
