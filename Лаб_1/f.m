function y = f( x )
    y = log(12 - 0.8 * x .^ 2) .* sin(2.6 + 0.3 * x.^2);
end
