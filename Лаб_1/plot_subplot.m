X = [-3.8 : 0.1 : 3.8];
Y = f(X);
figure;
subplot(2, 2, 1);
hold on;
plot(X, Y, '--');
for i = -3.8 : 0.3 : 3.8
 plot(i, f(i), 'r*', 'MarkerSize', 10);
end
title('ln(12 - 0.8x^2)sin(2.6+0.3x^2)');
xlabel('x');
ylabel('y');
subplot(2, 2, 2);
hold on;
plot(X, Y, 'color', 'r', 'LineWidth', 3);
for i = -3.8 : 0.7 : 3.8
 plot(i, f(i), 'gh', 'Color', [0.6 0.5 0.2], 'MarkerSize', 14);
end
title('ln(12 - 0.8x^2)sin(2.6+0.3x^2)');
xlabel('x');
ylabel('y');
X = [-3 : 0.1 : -0.5];
Y = polinom(X);
subplot(2, 2, 3);
hold on;
plot(X, Y, '-.');
for i = -3 : 0.3 : -0.5
 plot(i, polinom(i), 'gv', 'MarkerSize', 10);
end
title('x^3+5.6x^2+10.11x+5.8');
xlabel('x');
ylabel('y');
subplot(2, 2, 4);
hold on;
grid on;
plot(X, Y, ':', 'color', 'r');
for i = -3 : 0.2 : -0.5
 plot(i, polinom(i), 'ko', 'MarkerSize', 9);
end
title('x^3+5.6x^2+10.11x+5.8');
xlabel('x');
ylabel('y');